# README #

### The Lonely Mountain!###

The game Lonely Mountain can either be cloned directly or downloaded:
https://bitbucket.org/skotvold/group-3-game-programming/downloads


### What is this repository for? ###

* This is a game made in our game programming course to help us prepare to our finals.
* Version 1.1

### How do I get set up? ###

*  Easy! Clone or download this repository!
*  Click the exe file called (torque2D.exe)
*  And the game will start! 
*  To play the game you should be two players, but you can play the game yourself

* 	 Play With someone:
*	 One player start the server, read the port and then read find his IPv4 or IPv6 with CMD on windows.
*	 The other player click 'c' and open the console and write for example: connectToServer("192.168.0.105:28000");
*	 Remember the "" and use your IP and the port the other play neeed to tell you when he starts a new server
*	 To play alone:
	 
* 	 The game is not meant to be played alone but you can achieve this by opening two instances of the game.
*	 Click server on one of the windowm and join on another window, wait for the connection to happen
*	 Click play, mute your secondary window, if you do not want the sound to duplicate

### This is developed by ###


* Eskil Andre' Skotvold
* Jan-Henrik Thorsen
* Anders Gjelsvik
