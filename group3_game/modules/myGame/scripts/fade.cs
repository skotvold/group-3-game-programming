
// fade.cs


// Create the overlay which is used to fade | black bitmap | 
function createFadeMap(%image_name)
{
	 // Create the sprite.
    %obj = new Sprite(fadeObj);

    // Set the sprite as "static" so it is not affected by gravity.
    %obj.setBodyType( static );

	// set the position of the obj ( need to correspond with camera position)
    %obj.Position = "0 0";
	
	// set the size of the window
	%obj.setSize($SCREEN_WIDTH , $SCREEN_HEIGHT);
    
	// Set to the furthest obj layer.
    %obj.SceneLayer = 1;

    // Sets the image to use for our obj
    %obj.Image = %image_name;
	
   %obj.setDefaultRestitution(1);   
   %obj.SceneGroup = 1;
   %obj.setBlendAlpha(0);
  
  
    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %obj );
	return %obj;
}

// Fade sprites
//----------------------------------------------------------------------
// fade in and out an sprite object
function fadeObj::fadeInOut(%this, %alpha, %level){
	
	// set the blend alpha each time
	%this.setBlendAlpha(%alpha);
	
	// increment the alpha value
	%alpha += 0.05;

	//  if alpha not 1, then fade in
	if(%alpha <= 1)
	{
		%this.fadeInEvent=%this.schedule(100,"fadeInOut",%alpha, %level);
		
	// when fade in is done, then  fade out after 2 seconds
	}else{
		if(%level == 1) {
			waitToLoadLevel(1);
		} else {
			waitToLoadMenu(3);
		}
		//%this.fadeOutEvent=%this.schedule(2000,"fadeOut",0.95);  
	}
}


// fade out an sprite object
function fadeObj::fadeOut(%this, %alpha){
	%this.setBlendAlpha(%alpha);
	%alpha -= 0.05;
	
	if(%alpha >= 0)
	{
		%this.fadeOutEvent=%this.schedule(100,"fadeOut",%alpha);
	} else {
		level::createLevel_one($SCREEN_WIDTH, $SCREEN_HEIGHT,"ToyAssets:skyBackground");
	}
}
