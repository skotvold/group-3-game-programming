// electron_player.cs

// global speed values
$PLAYER_SPEED_X = 30;
$PLAYER_SPEED_Y = 25;

// players gold count
$GOLD_COUNT = 0;

// Speed boost | default 0 | 
$ADD_SPEEDBOOST = 0;





function createDwarfPlayer(%startPosX, %startPosY){

    $lightBulbHit = false; // turn true when player is at light bulb.
   
   // Create the sprite.
    %player = new Sprite(PlayerDwarf);

    // We want our dwarf to move and be affected by gravity and various forces
    // so we set its BodyType to dynamic
    %player.setBodyType( dynamic );

    // Set the start position of the player
    %player.setPosition(%startPosX , %startPosY);

    // Set the size of the player        
    %player.Size = "4 4";

    // Set the layer closest to the camera (above the background)
    %player.SceneLayer = 1;

    // Set image/animation of the player
    %player.Animation = "ToyAssets:TD_DwarfWalkSouth";
   
   // This creates a box which so that collisions with the screen edges register properly
    // Calling createPolygonBoxCollisionShape() without arguments sets the box to the size of the 
    // sceneobject automatically.
    %player.createPolygonBoxCollisionShape();

    // Add the sprite to the scene.
    mySceneWindow.getScene().add( %player );  
	 
    %player.setCollisionCallback( true );    

    // setFixedAngle prevents the player's rotation from being 
	// influenced by Collisions and physics forces
    %player.setFixedAngle(true);
	
	// initalize a gold text
	setGoldText();
    
  
}

function PlayerDwarf::onCollision(%this, %sceneobject, %collisiondetails)
{

	// If we collide with something from scenegroup1
	// this is the border collision, we make collide effect sound
	if(%sceneobject.getSceneGroup() == 1)
	{
		
		// stop the player if he hits the borders or a wall
		%this.SetLinearVelocityX(0);
		%this.SetLinearVelocityY(0);
		
		
	}

   // if we collide with something from scenegroup 2 (goal)
   // we swap out the unlit light bulb with the lit one. 
   if(%sceneobject.getSceneGroup() == 2){

        createEndScreen(100, 100); 
        commandToServer('ImDone');
      
    }

	if(%sceneobject.getSceneGroup() == 3)
	{
		destroySpeedUP(%sceneobject);
		waitForSpeedUp(3);
		$ADD_SPEEDBOOST = 20;
		
		// Need discussion 
		// need to update
		%this.stopMoveX(0);
		%this.stopMoveY(0);
		audioManager::playSE("myGame:speedBoost");
		
	}
	
	// if you collide with gold
	if(%sceneobject.getSceneGroup() == 4)
	{
		if ($GOLD_COUNT < 40) {
			audioManager::playSE("myGame:getGold");
			destroyGold(%sceneobject);
			$GOLD_COUNT+=2;
			setGoldText();
			echo($GOLD_COUNT);
		}
	}
}


// print the players gold count
function setGoldText()
{
	// destroy gold text
	$g_goldText.delete();
	%text = "Gold: " @$GOLD_COUNT;
	$g_goldText = makeGuiText(%text, "0 20" , "150 30");
}


// Set the movement in different directions
// left, right, up and down

function PlayerDwarf::moveLeft(%this)
{	
	%this.setFlipX(false);
	%this.Animation = "ToyAssets:TD_DwarfWalkWest";
	%this.SetLinearVelocityX( -1* ( $PLAYER_SPEED_X + $ADD_SPEEDBOOST - ($GOLD_COUNT / 2) - $SLOW_DOWN) );
}

function PlayerDwarf::moveRight(%this)
{
	%this.Animation = "ToyAssets:TD_DwarfWalkWest";
	%this.setFlipX(true);
	%this.SetLinearVelocityX( $PLAYER_SPEED_X + $ADD_SPEEDBOOST - ($GOLD_COUNT / 2) - $SLOW_DOWN);
}

function PlayerDwarf::moveUp(%this)
{
	%this.Animation = "ToyAssets:TD_DwarfWalkNorth";
	%this.SetLinearVelocityY( $PLAYER_SPEED_Y + $ADD_SPEEDBOOST - ($GOLD_COUNT / 2) - $SLOW_DOWN);
}

function PlayerDwarf::moveDown(%this)
{
	%this.Animation = "ToyAssets:TD_DwarfWalkSouth";
	%this.SetLinearVelocityY( -1 * ( $PLAYER_SPEED_Y + $ADD_SPEEDBOOST - ($GOLD_COUNT / 2) - $SLOW_DOWN) );
}


// Stop the movement in X and Y direction
function PlayerDwarf::stopMoveX(%this)
{ 
	%this.SetLinearVelocityX(0);
}

function PlayerDwarf::stopMoveY(%this)
{ 
	%this.SetLinearVelocityY(0);
}