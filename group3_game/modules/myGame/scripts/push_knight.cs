// push_knight.cs


function createPushEnemy(%startPosX, %startPosY , %speedX , %speedY){
   
   // Create the sprite.
    %enemy = new Sprite(enemy_knight);

    // so we set its BodyType to dynamic
    %enemy.setBodyType( kinematic );

    // Set the start position of the enemy
    %enemy.setPosition(%startPosX , %startPosY);

    // Set the size of the enemy        
    %enemy.Size = "3 3";

    // Set the layer closest to the camera (above the background)
    %enemy.SceneLayer = 1;

    // Set image of the enemy
    %enemy.Animation = "ToyAssets:TD_Knight_MoveSouth";
   
   // This creates a box which so that collisions with the screen edges register properly
    %enemy.createPolygonBoxCollisionShape();

    // Add the sprite to the scene.
    mySceneWindow.getScene().add( %enemy ); 

	// set enemies collision callback
    %enemy.setCollisionCallback( true );

	// Set the speed in x and y direction
	%enemy.SetLinearVelocityX(%speedX);
	%enemy.SetLinearVelocityY(%speedY);
	
	// set the restution when colliding, example: player
	%enemy.setDefaultRestitution(0);
	return %enemy;
}

// Will spawn multiple spawn objects of the push object
function spawnPushObj(%posX , %posY , %speedX, %speedY , %interval)
{	
	createPushEnemy(%posX , %posY , %speedX, %speedY);
	schedule(%interval,0,spawnPushObj, %posX , %posY , %speedX, %speedY , %interval);
}

