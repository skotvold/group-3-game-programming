//---------------------------------------------------------------------------------------------
// GameConnection.onConnectRequest
// This script function is called before a client connection is accepted.  Returning "" will
// accept the connection, anything else will be sent back as an error to the client. All the
// connect args are passed also to onConnectRequest.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectRequest(%client, %netAddress) {
   echo("Connect request from: " @ %netAddress);
   return "";
}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnect
// This function is called when a client connection is accepted.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnect(%client, %name) {
   if ($Server::PlayerCount < 2) {
      // Send down the connection error info. The client is responsible for displaying this
      // message if a connection error occures.
      commandToClient(%client, 'SetConnectionError', $Pref::Server::ConnectionError);
   
      %client.setPlayerName(%name);
      echo("Client Connected: " @ %client @ " " @ %client.getAddress());
      $Server::PlayerCount++;
   
      // Tell the new guy about everyone else.
      %count = ClientGroup.getCount();
      for (%i = 0; %i < %count; %i++) {
         %other = ClientGroup.getObject(%i);
         commandToClient(%other, 'Update', $Server::PlayerCount);
      }

      if ($Server::PlayerCount == 2) {
         gameIsReady();
      }
   }
}

//---------------------------------------------------------------------------------------------
// GameConnection.onDrop
// This function is called when a client drops for any reason.
//---------------------------------------------------------------------------------------------
function GameConnection::onDrop(%client, %reason) {
   removeTaggedString(%client.name);
   echo("Client Dropped: " @ %client @ " " @ %client.getAddress());
   $Server::PlayerCount--;

   %count = ClientGroup.getCount();
   
   for (%i = 0; %i < %count; %i++) {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'OpponentDropped');
   }
}

//---------------------------------------------------------------------------------------------
// GameConnection.setPlayerName
// Sets %clients name based on %name. If %name is already used, a number is appended or
// incremented until the name is unique.
//---------------------------------------------------------------------------------------------
function GameConnection::setPlayerName(%client, %name) {
   echo("name = " @ %name);
   
   // If nothing was passed, set a default name.
   %name = trim(%name);
   if (%name $= "")
      %name = "T2D Gamer";

   // Make sure the name is unique, we'll hit something eventually.
   %nameTest = %name;
   %count = 0;
   while(!isNameUnique(%nameTest))
   {
      %count++;
      %nameTest = %name @ %count;
   }

   // Set the name.
   %client.name = %nameTest;
}

//---------------------------------------------------------------------------------------------
// isNameUnique
// Checks if %name is not used by any clients.
//---------------------------------------------------------------------------------------------
function isNameUnique(%name) {
   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++) {
      %client = ClientGroup.getObject(%i);
      %test = %client.name;
      if (strcmp(%name, %test) == 0) {
         return false;
      }
   }
   
   // If we get here, the name is unique.
   return true;
}

// Tells the players if they won or lost
function updateConnectedPlayers(%client) {
   %count = ClientGroup.getCount();
   
   for (%i = 0; %i < %count; %i++) {
      %c = ClientGroup.getObject(%i);
      if (%c != %client) {
         commandToClient(%c, 'GameOver');
      } else {
         commandToClient(%c, 'YouWon');
      }
   }
}

// Tells the players that the game is ready to start
function gameIsReady() {
   %count = ClientGroup.getCount();
   
   for (%i = 0; %i < %count; %i++) {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'GameIsReady');
   }
}

// Starts the game for both players
function startGame() {
   %map = getMap();
   %count = ClientGroup.getCount();
   echo(%map);
   
   for (%i = 0; %i < %count; %i++) {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'StartGame', %map);
   }
}

// Tells the players when the other player rage quits
function rageQuit(%client) {
   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++) {
      %c = ClientGroup.getObject(%i);
      if(%client != %c) {
         commandToClient(%c, 'OpponentReturnedToMainMenu');
      } else {
         commandToClient(%c, 'YouReturnedToMainMenu');
      }
   }
}

// Tells both players that they lost / ran out of time
function gameTimedOut() {
   %count = ClientGroup.getCount();
   
   for (%i = 0; %i < %count; %i++) {
      %client = ClientGroup.getObject(%i);
      commandToClient(%client, 'YouAreTooSlow');
   }
}

// Returns the map to play
function getMap() {
    %maxMap = getNumberOfMaps();
    echo(getRandom(1,%maxMap));
    return "level" @ getRandom(1,%maxMap) @ ".txt";
}

// Returns the number of level-files that exists in the folder
function getNumberOfMaps() {
    %numberOfFiles = 0;
    %file = new FileObject();
    %i = 1;
    
    while(true) {
        %level = "level" @ %i @ ".txt";
        if(%file.openForRead("modules/myGame/files/maps/" @ %level)) {
            %numberOfFiles += 1;
            
            %file.close();
            %i += 1;
        } else {
            %file.delete();
            break;
            
        }
    }
    return %numberOfFiles;
}

// Slows the opponent down
function slowOpponentDown(%client) {
   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++) {
      %c = ClientGroup.getObject(%i);
      if(%client != %c) {
         commandToClient(%c, 'SlowMeDown');
      }
   }
}

// Blocks the opponents goal
function blockOpponentGoal(%client) {
   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++) {
      %c = ClientGroup.getObject(%i);
      if(%client != %c) {
         commandToClient(%c, 'BlockMyGoal');
      }
   }
}