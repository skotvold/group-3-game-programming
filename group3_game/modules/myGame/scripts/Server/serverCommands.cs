// Pleyer has reached goal
function serverCmdImDone(%client) {
	updateConnectedPlayers(%client);
}

// Start the game
function serverCmdStartGame() {
	startGame();
}

// Client has rage quitted
function serverCmdRageQuit(%client) {
	rageQuit(%client);
}

// Timer ran out, both players lost
function serverCmdTimeOut() {
	gameTimedOut();
}

// Slow the opponent down
function serverCmdSlowDown(%client) {
	slowOpponentDown(%client);
}

// Block opponents goal
function serverCmdBlockGoal(%client) {
	blockOpponentGoal(%client);
}