
// main_menu.cc

// create the main menu
function menu::create(%width,%height)
{
	// create the menu background [background.cs , first function]
	background::create(%width,%height,"myGame:menu_bg");
	
	// play background music [audio.cs , first function]
	audioManager::playBackground("myGame:introTrack" , audioManager::getVolume(5));
	
	// Create the menu buttons [menu_button.cs , first function]
	// [pos at X, pos at Y, size in X dir, size in Y dir , scene group number , name]
	createMenuButton(0, 20, 45, 7, 6, "myGame:NewGameButton");
	createMenuButton(0, 10, 45, 7, 9, "myGame:serverButton");
	createMenuButton(0, 0, 45, 7, 10, "myGame:joinButton");
	createMenuButton(0, -10, 45, 7, 12, "myGame:instructionButton");
	createMenuButton(0, -20, 45, 7, 7, "myGame:highScoreButton");
	createMenuButton(0, -30, 45, 7, 8, "myGame:QuitButton");

	// display the amount of players
	if ($serverLocal) {
		displayPlayersConnected($playersConnected);
	}

	$allClientsConnected = false;

	// destroy the player control in menu [controls.cs ,5th function]
	//InputManager::destroyPlayerControl();
	
	// attach the menu control in menu [controls.cs , 3th function}
	InputManager::Init_playerControls();
	
	
	// input safety check
	mySceneWindow.UseObjectInputEvents = true;  
	mySceneWindow.getScene().UseObjectInputEvents = true;
	

}

// Destroy the menu
function menu::destroy()
{
	// Finish if no scene available.
    if ( !isObject(mySceneWindow.getScene()) )
	{
        return;
	}
	
	// delete the scene window,
	mySceneWindow.getScene().delete();

	// delete the connection between players
	if(isObject($con)){
		$con.delete();
	}
	
}


// start a specific level
function menu::startLevel( %width , %height )
{
	
	// create a new scene to set to the scene window
	%scene = new Scene (newScene);
	mySceneWindow.setScene(%scene);
	
	// create the preferred level
	level::createLevel(%width,%height,"myGame:level_one");
	
}

// show high score
function menu::showHighscore(%width, %height) {
	%scene = new Scene (newScene);
	mySceneWindow.setScene(%scene);
	
	// create the high score [highscore.cs , first function]
	highscore::create_highscore(%width, %height);
}

// show high score
function menu::showInstructions(%width, %height) {
	%scene = new Scene (newScene);
	mySceneWindow.setScene(%scene);
	
	// create the high score [highscore.cs , first function]
	instructions::create_instructions(%width, %height);
}