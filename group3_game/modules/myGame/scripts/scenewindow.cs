function createSceneWindow(%width,%height)
{
	// if scene window doesn't exist
    if ( !isObject(mySceneWindow) )
    {
        // Create the scene window.
        new SceneWindow(mySceneWindow);

       // Set Gui profile. If you omit the following line, the program will still run as it uses                
       // GuiDefaultProfile by default

        mySceneWindow.Profile = GuiDefaultProfile;

        // Place the sceneWindow on the Canvas
        Canvas.setContent( mySceneWindow );                     
    }
		
		
	// The camera details of the program
    mySceneWindow.setCameraPosition( 0, 0 );		// Set the position of the camera
    mySceneWindow.setCameraSize( %width, %height );	// Set the size of the camera
    mySceneWindow.setCameraZoom( 1 );				// Set the zoom of the camera
    mySceneWindow.setCameraAngle( 0 );				// Angle of the camera
	
	
}

function destroySceneWindow()
{  
	// Finish if no scene available.
    if ( !isObject(mySceneWindow) )
	{
        return;
	}
	
	// delete the scene window
	mySceneWindow.delete();
}

// Set the camera to mount on the level
function setCameraLevelSettings(%zoom)
{
	// here we set new camera zoom
	mySceneWindow.setCameraZoom(%zoom);
	
	// mount the camera on top of the player
	mySceneWindow.mount(PlayerDwarf,"0 0",3,true);

}

// Set the camera to reset back from a mounted state
function resetCamera()
{
	// dismount the camera
	mySceneWindow.dismountMe(PlayerDwarf);
	
	// set back the zoom
	mySceneWindow.setCameraZoom( 1 );
	
	// set position of the camera
	mySceneWindow.setPosition( 0 , 0 );
}
