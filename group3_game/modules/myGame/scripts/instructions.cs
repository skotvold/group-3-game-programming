// instruction.cs

// Create the instructions
function instructions::create_instructions(%width , %height) {
	// create the background [background.cs , first function]
	%obj = background::create(%width,%height,"myGame:menu_bg");
	
	// set new alpha on the player
	%obj.setBlendAlpha(0.7);
	
	// Create a gui text to represent the highscore [guiText.cs , first function]
	$instruction[0] = makeGuiText("INSTRUCTIONS", "450 60", "150 30");
	$instruction[1] = makeGuiText("You are a lost dwarf.", "420 200", "900 30");
	$instruction[2] = makeGuiText("Your goal is to reach The Lonely Mountain to meet up with all the other dwarves.", "90 225", "900 30");
	$instruction[3] = makeGuiText("But there is just room for one more dwarf.", "310 250", "900 30");
	$instruction[4] = makeGuiText("You need to get there before the other lost dwarf and before the time runs out!", "100 275", "900 30");
	$instruction[5] = makeGuiText("On the road you will find gold and power-ups.", "295 300", "900 30");
	$instruction[6] = makeGuiText("Holding gold will slow you down, but you can also use gold to hinder the other dwarf.", "70 325", "900 30");
	$instruction[7] = makeGuiText("By pressing 'space' you can use 5 gold coins to slow your opponent for 3 seconds.", "95 350", "900 30");
	$instruction[8] = makeGuiText("By pressing 'e' you can create a wall in front of your opponents goal for 5 seconds.", "93 375", "900 30");
	$instruction[9] = makeGuiText("The power-up gives you a boost for 3 seconds", "285 400", "900 30");
	$instruction[10] = makeGuiText("Begin your journey to The Lonely Mountain!", "297 450", "900 30");

	createMenuButton( 0, -25, 45, 7, 11, "myGame:backButton");
}