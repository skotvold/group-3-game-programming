// gold.cs

// Create normal golds
function createGold(%posX, %posY){   
	// Create the sprite.
    %gold = new Sprite();

	// set collision body to static
    %gold.setBodyType( dynamic );

    // Set the position.
    %gold.setPosition(%posX , %posY);

    // Set the size.        
    %gold.setSize(3 , 3);

    // Set to the front layer.
    %gold.SceneLayer = 1;
    
    %gold.Image = "ToyAssets:TD_GoldSprite";

    //Sets the collision shape to a polygon
    %gold.createPolygonBoxCollisionShape();
	
	%gold.SceneGroup = 4;

    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %gold );
}

function destroyGold(%obj)
{
	%obj.safedelete();
}

