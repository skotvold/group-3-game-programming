//wait.cs

// utility script which contain different 
// wait functions

// wait tt load menu
function waitToLoadMenu(%sec) {
	%sec -= 1;
	if(%sec >= 0) {
		schedule(1000, 0 ,"waitToLoadMenu", %sec);
	} else {
		loadMenu(1);
	}
}

// wait to load level
function waitToLoadLevel(%sec) {
	%sec -= 1;
	if(%sec >= 0) {
		schedule(1000, 0 ,"waitToLoadLevel", %sec);
	} else {
		loadLevel(1);
	}
}

// wait to destroy any gui text
function waitForDestroyText(%sec) {
	%sec -= 1;
	if(%sec >= 0 && $stillActive) {
		schedule(1000, 0 ,"waitForDestroyText", %sec);
	} else {
		if(isObject($DM)){
			$DM.delete();
			$stillActive = false;
		}
	}
}

// wait for speedUP to finish
function waitForSpeedUp(%sec) {
	%sec -= 1;
	if(%sec >= 0 ){
		schedule(1000, 0 ,"waitForSpeedUp",%sec);
	}else{
		$ADD_SPEEDBOOST = 0;
	}
}

// Wait for slow down to finish
function waitForSlowDown(%sec) {
	%sec -= 1;
	if(%sec >= 0 ){
		schedule(1000, 0 ,"waitForSlowDown",%sec);
	}else{
		$SLOW_DOWN = 0;
	}
}

// Wait for goal block to finish
function waitForBlockGoal(%sec, %wallBlock) {
	%sec -= 1;
	if(%sec >= 0 ){
		schedule(1000, 0 ,"waitForBlockGoal",%sec, %wallBlock);
	}else{
		removeBlock(%wallBlock);
	}
}