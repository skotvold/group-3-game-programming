 
// background.cs
// Create the background

function background::create(%width,%height, %image_name)
{   // Create the sprite.
    %background = new Sprite();

    // Set the sprite as "static" so it is not affected by gravity.
    %background.setBodyType( static );

	// set the position of the background ( need to correspond with camera position)
    %background.Position = "0 0";
	
	// set the size of the window
	%background.setSize(%width , %height);
    
	// Set to the furthest background layer.
    %background.SceneLayer = 31;

    // Sets the image to use for our background
    %background.Image = %image_name;

	// set the background alpha to 1
    %background.setBlendAlpha(1);

    // Create border collisions.
   %background.createEdgeCollisionShape( -%width/2 , -%height/2 , -%width/2 ,  %height/2 );	// Left
   %background.createEdgeCollisionShape(  %width/2 , -%height/2 ,  %width/2 ,  %height/2 );	// Right
   %background.createEdgeCollisionShape( -%width/2 ,  %height/2 ,  %width/2 ,  %height/2 );	// top 
   %background.createEdgeCollisionShape( -%width/2 , -%height/2 ,  %width/2 , -%height/2 );	// down
	
	// set default restitution
   %background.setDefaultRestitution(1);   
   
   // set the background to scene group 1
   %background.SceneGroup = 1;

    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %background );
	return %background;
 
}


