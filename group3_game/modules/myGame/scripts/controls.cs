// controls.cs

// Handle the input - not necessary
function InputManager::onTouchDown(%this, %touchId, %worldposition)
{    
	//We assign the list of objects that were hit to variable %picked
	
	%picked = mySceneWindow.getScene().pickPoint(%worldposition);
	
	//%picked.count is the number of items listed in the %picked variable
	for(%i=0; %i<%picked.count; %i++)
	{

		//When iterating through the list, getWord will return item number %i in variable %picked
		%myobj = getWord(%picked,%i);

		//If this item belongs to SceneGroup N
		if(%myobj.getSceneGroup() == 6)
		{
			// game on preferred map
			if($allClientsConnected == true && $serverLocal == true) {
				commandToServer('StartGame');
			}
		}
		
		else if(%myobj.getSceneGroup() == 7)
		{
			// destroy the menu and show highscore
			menu::destroy();
			menu::showHighscore($SCREEN_WIDTH, $SCREEN_HEIGHT);
		}
		
		else if(%myobj.getSceneGroup() == 8)
		{
			// Quit the game
			myGame::destroy();
		}
		
		// create a server
		else if(%myobj.getSceneGroup() == 9)
		{
			if($serverLocal == false && $serverConnected == false) {
				createServer(true);
			}
		}

		// Try to connect to a server
		else if(%myobj.getSceneGroup() == 10)
		{
			if($serverLocal == false && $serverConnected == false) {
				createTimer();
				connectToServer("127.0.0.1:" @ $pref::Net::Port);
				startTimer(15, "Trying to join server: ", "Connection timed out." ,"0 0" ,"750 30");
			}
		}

		else if(%myobj.getSceneGroup() == 12)
		{
			// destroy the menu and show instructions
			menu::destroy();
			menu::showInstructions($SCREEN_WIDTH, $SCREEN_HEIGHT);
		}
		
		// Load menu
		else if(%myobj.getSceneGroup() == 11)
		{
			loadMenu(1);
		}
	}   
}

//--------------------------------------------------------------------------------

// Initalize input from keyboard when playing
function InputManager::Init_playerControls(%this)
{
	//Create our new ActionMap
	new ActionMap(playerControls);

	// Normal wasd setup for moving the electron/player
	playerControls.bindCmd(keyboard, "a", "PlayerDwarf.moveLeft();" , "PlayerDwarf.stopMoveX();");
	playerControls.bindCmd(keyboard, "d", "PlayerDwarf.moveRight();", "PlayerDwarf.stopMoveX();");
	playerControls.bindCmd(keyboard, "w", "PlayerDwarf.moveUp();"   , "PlayerDwarf.stopMoveY();");
	playerControls.bindCmd(keyboard, "s", "PlayerDwarf.moveDown();" , "PlayerDwarf.stopMoveY();");
	
	// pause the game with p
	PlayerControls.bind(keyboard, "p", "pressPause");
	
	// controls for the managing the audio ingame
	PlayerControls.bind(keyboard, "m", "toggleMute");
	PlayerControls.bind(keyboard, "-", "volumeDown");
	PlayerControls.bind(keyboard, "+", "volumeUp");
	PlayerControls.bind(keyboard, "q", "IAmAngry");
	PlayerControls.bind(keyboard, "space", "slowOpponent");	
	PlayerControls.bind(keyboard, "e", "blockOpponent");
	PlayerControls.bind(keyboard, "c", "toggleConsole");
	

	//Push our ActionMap on top of the stack
	playerControls.push();

}


// Controls the action in the menu
/*
function InputManager::initMenuControl(%this)
{
	//Create our new ActionMap
	new ActionMap(menuControl);
	
	// controls for the managing the audio ingame
	menuControl.bind(keyboard, "m", "toggleMute");
	menuControl.bind(keyboard, "-", "volumeDown");
	menuControl.bind(keyboard, "+", "volumeUp");
	
	
	//Push our ActionMap on top of the stack
	menuControl.push();

}

function InputManager::destroyMenuControl(%this)
{
	// pop the action map
	menuControl.pop();
}

function InputManager::destroyPlayerControl(%this)
{
	// pop the action map
	playerControls.pop();
}
*/