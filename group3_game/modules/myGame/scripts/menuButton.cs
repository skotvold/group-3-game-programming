
// menuButton.cs

// Create normal buttons | CHANGE the type from asteroid to a normal button
function createMenuButton(%posX, %posY, %sizeX, %sizeY , %sceneGroupNum , %name)
{   
	// Create the sprite.
    %button = new Sprite();

    %button.setBodyType( static );

    // Set the position.
    %button.setPosition(%posX , %posY);

    // Set the size.        
    %button.setSize(%sizeX , %sizeY);

    // Set to the frontmost layer.
    %button.SceneLayer = 1;

    //Here we assign all created buttons to SceneGroup 20. More on this in Chapter 11.
    %button.SceneGroup = %sceneGroupNum;
		
	%button.Image = %name;

    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %button );
}