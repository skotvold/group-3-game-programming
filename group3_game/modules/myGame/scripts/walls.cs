
// walls.cs

// Create normal walls
function createNormalWall(%posX, %posY, %sizeX, %sizeY ){   
	// Create the sprite.
    %wall = new Sprite();

	// set collision body to static
    %wall.setBodyType( static );

    // Set the position.
    %wall.setPosition(%posX , %posY);

    // Set the size.        
    %wall.setSize(%sizeX , %sizeY);

    // Set to the front layer.
    %wall.SceneLayer = 1;
    
    %wall.Image = "myGame:wall";

    //Sets the collision shape to a polygon
    %wall.createPolygonBoxCollisionShape();

    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %wall );

    return %wall;
}

// create a up wall
function createUpWall(%posX, %posY, %sizeX, %sizeY ){   
	// Create the sprite.
    %wall = new Sprite();

	// set collision body to static
    %wall.setBodyType( static );

    // Set the position.
    %wall.setPosition(%posX , %posY);

    // Set the size.        
    %wall.setSize(%sizeX , %sizeY);

    // Set to the front layer.
    %wall.SceneLayer = 1;
    
    // set image of wall
    %wall.Image = "myGame:wallup";
   
    // set image of polygonBox
    %wall.createPolygonBoxCollisionShape();

    //%wall.setImageFrame();
	%wall.SceneGroup = 1;
   
   // Add the sprite to the scene.
	mySceneWindow.getScene().add( %wall );
}



// and now we can add other types of wall, etc etc
// function createExampleWall(){}

