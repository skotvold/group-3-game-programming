
// level.cs
// Create the level
function level::createLevel(%width , %height , %backgroundName) {
 	// Variables used by level
 	%wall = false;
 	%gold = false;
 	%power = false;
 	%block = false;
 	$count = 0;
 	
	// stop the audio
	audioManager::stopAudio();
	$inGame = true;

    $GOLD_COUNT = 0;

	// create the level background
	background::create(%width,%height,%backgroundName);

	// read in level from file
    %file = new FileObject();
    if(%file.openForRead("modules/myGame/files/maps/" @ $level)) {
    	// Read player start position
	   	%line = %file.readLine();
        %playerPos = explode(%line, ",");
        createDwarfPlayer(%playerPos.contents[0], %playerPos.contents[1]);
        
        // Read goal position
        %line = %file.readLine();
        %goalPos = explode(%line, ",");
        createMountain(%goalPos.contents[0],
        			   %goalPos.contents[1],
        			   %goalPos.contents[2],
        			   %goalPos.contents[3]);
        
        // Sets the timer for the map
        %timer = %file.readLine();

        while (!%file.isEof()) {
            %line = %file.readLine();
            if (%line $= "#wall") {
            	%wall = true;
            	%gold = false;
            	%power = false;
            	%block = false;
            	continue;
            } else if (%line $= "#gold") {
            	%gold = true;
            	%wall = false;
            	%power = false;
            	%block = false;
            	continue;
            } else if (%line $= "#power") {
            	%power = true;
            	%wall = false;
            	%gold = false;
            	%block = false;
            	continue;
            } else if (%line $= "#block") {
            	%block = true;
            	%power = false;
            	%wall = false;
            	%gold = false;
            	continue;
            }

            if (%wall) {
            	// Sets the positions of the walls
            	%mapPositions = explode(%line, ",");
            
            	createNormalWall(%mapPositions.contents[0], 
                             	 %mapPositions.contents[1], 
                             	 %mapPositions.contents[2], 
                             	 %mapPositions.contents[3]);
            } else if (%gold) {
            	// Sets the positions of the gold
            	%goldPosition = explode(%line, ",");

            	createGold(%goldPosition.contents[0],
            			   %goldPosition.contents[1],
            			   %goldPosition.contents[2],
            			   %goldPosition.contents[3]);
            } else if (%power) {
            	// Sets the positions of the power ups
            	%speedUp = explode(%line, ",");

            	createSpeedUP(%speedUp.contents[0],
            				  %speedUp.contents[1],
            				  %speedUp.contents[2],
            				  %speedUp.contents[3]);
            } else if (%block) {
            	// Saves the value of the blocking walls in an array
            	$savedGoalBlock[$count] = %line;
            	$count += 1;
            }
        }

    }
    
    %file.close();
	%file.delete();
	
	// adds music to play in the background [background.cs , first function ]
	audioManager::playBackground("myGame:level1" , audioManager::getVolume(5));	
	
	// init player conrols [controls.cs, 2th function]
	InputManager::Init_playerControls();
	
	// zoom in camera [scenewindow.cs , 2th function]
    setCameraLevelSettings(3.0);
	
	// create the timer[timer.cs , first function]
	createTimer();
	
	// start the timer [timer , 2th function]
	startTimer(%timer, "Time: ", "Time's up!", "0 0"  ,"150 30");
	
	// NOT SURE WHAT THIS IS DOING!
	makeGuiText(); 	
}

function level::destroyLevel() {
	// Finish if no scene available.
    if (!isObject(mySceneWindow.getScene())) {
        return;
	}
	
	// stop the audio [audio.cs , 7th function] 
	audioManager::stopAudio();
	mySceneWindow.getScene().delete();
	
	// destroy the timer [timer.cs, 3th function]
    destroyTimer();
	
	// create a new scene
	new Scene (mainScene) {  
        Modal = true;     
    };  
  
    new SceneWindow (mySceneWindow) {
  
    };  
   
	mySceneWindow.profile = new GuiControlProfile() {
		Modal = true;
	};
	
    mySceneWindow.setScene(mainScene);  
    mySceneWindow.UseObjectInputEvents = true;  
    mainScene.UseObjectInputEvents = true;  
	
	// The camera details of the program
    mySceneWindow.setCameraPosition( 0, 0 );						// Set the position of the camera
    mySceneWindow.setCameraSize($SCREEN_WIDTH, $SCREEN_HEIGHT);	    // Set the size of the camera
    mySceneWindow.setCameraZoom(1); 								// Set the zoom of the camera
    mySceneWindow.setCameraAngle(0);								// Angle of the camera
      
    Canvas.setContent(mySceneWindow);  
	
	new ScriptObject(InputManager);
    mySceneWindow.addInputListener(InputManager);	
}

// Creates a block for the local user
function createBlock() {
	for(%i = 0; %i < $count; %i++) {
		%blockade = explode($savedGoalBlock[%i], ",");
		%wallBlock = createNormalWall(%blockade.contents[0],
									  %blockade.contents[1],
									  %blockade.contents[2],
									  %blockade.contents[3]);

		waitForBlockGoal(5, %wallBlock);
	}
}

// Removes the block for the local user
function removeBlock(%wallBlock) {
	if(isObject(%wallBlock)){
		%wallBlock.delete();
	}
}