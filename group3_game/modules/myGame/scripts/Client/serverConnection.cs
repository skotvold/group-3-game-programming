//---------------------------------------------------------------------------------------------
// SetConnectionError
// Upon connecting to a server, the server will call this function on the client to describe a 
// connection error. If there is no error, this will still be called, but %error will be
// empty. All we do here is save the error message for display when the server reports the
// actual connection error.
//---------------------------------------------------------------------------------------------
function clientCmdSetConnectionError(%error) {
   $ServerConnectionErrorMessage = %error;
}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionAccepted
// Called when a connection is established and accepted.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectionAccepted(%this) {

}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionTimedOut
// A connection was established with the server, but has since timed out.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectionTimedOut(%this) {
   $clientMessage = "Your opponent was disconnected.";
   disconnectedCleanup();
}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionTimedOut
// A connection was established with the server, but has since been dropped.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectionDropped(%this, %msg) {
   $clientMessage = "Your opponent was disconnected.";
   disconnectedCleanup();
}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionTimedOut
// A connection was established with the server, but some error (sent to 'SetConnectionError')
// has occured.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectionError(%this, %msg) {
   $clientMessage = "Your opponent was disconnected.";
   disconnectedCleanup();
}


//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionTimedOut
// A connection was not able to be made with the server because the server rejected it.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectRequestRejected(%this, %msg) {
   switch$(%msg) {
      case "CHR_PASSWORD":
         if ($Client::Password $= "") {
            %error = "The server requires a password.";
         } else {
            %error = "The password you entered is incorrect.";
         }
            
      case "CHR_PROTOCOL":
         %error = "Incompatible protocol version: Your game version is not compatible with this server.";
         
      default:
         %error = "Connection error. Please try another server. Error code: (" @ %msg @ ")";
   }
   
   $clientMessage = "Your request was rejected by the server.";
   disconnectedCleanup();
}

//---------------------------------------------------------------------------------------------
// GameConnection.onConnectionRequestTimedOut
// A connection was not able to be made with the server and the request timed out.
//---------------------------------------------------------------------------------------------
function GameConnection::onConnectRequestTimedOut(%this) {
   $clientMessage = "Connection could not be established.";
   disconnectedCleanup();
}