// Write the highscores for the player to a local file for now.
// This is not safe as the player could very well change the values by them selves
function writeToFile() {
	%file = new FileObject();

	if(%file.openForWrite("modules/myGame/files/results.txt")) {
		%file.writeLine($iHaveWon);
		%file.writeLine($iHaveWonByOpponentRageQuit);
		%file.writeLine($iHaveLost);
		%file.writeLine($iHaveLostByRageQuit);
		%file.writeLine($iHaveLostByBeingTooSlow);
	}

	%file.close();
	%file.delete();
}

// Reads the highscores for the player from a local file
// Still not safe to keep the highscore on a local file...
function readFromFile() {
	%file = new FileObject();

	if(%file.openForRead("modules/myGame/files/results.txt")) {
		$iHaveWon = %file.readLine();
		$iHaveWonByOpponentRageQuit = %file.readLine();
		$iHaveLost = %file.readLine();
		$iHaveLostByRageQuit = %file.readLine();
		$iHaveLostByBeingTooSlow = %file.readLine();
	} else {
		$iHaveWon = 0;
		$iHaveWonByOpponentRageQuit = 0;
		$iHaveLost = 0;
		$iHaveLostByRageQuit = 0;
		$iHaveLostByBeingTooSlow = 0;
	}

	%file.close();
	%file.delete();
}