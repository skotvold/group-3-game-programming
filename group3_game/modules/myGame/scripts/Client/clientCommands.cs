// Updates info for the connected users
function clientCmdUpdate(%playerCount) {
	displayPlayersConnected(%playerCount);
	destroyTimer();
}

// Takes care of a player losing
function clientCmdGameOver() {
	$inGame = false;
	$iHaveLost += 1;
	writeToFile();
	
	audioManager::stopAudio();
	audioManager::setChannelVolume(10,1.0);
	audioManager::playSE("myGame:lose");
	destroyTimer();
	
	makeGuiText("You lost!", "450 650", "150 30");
	globalFade("myGame:blackBox", 0);
}

// Takes care of a player winning
function clientCmdYouWon() {
	$inGame = false;
	$iHaveWon += 1;
	writeToFile();
	makeGuiText("You won!", "450 650", "150 30");
	
	audioManager::stopAudio();
	audioManager::setChannelVolume(10,1.0);
	audioManager::playSE("myGame:win");
	destroyTimer();

	globalFade("myGame:blackBox", 0);
}

// Takes care of a player losing due to game timed out
function clientCmdYouAreTooSlow() {
	$inGame = false;
	$iHaveLostByBeingTooSlow += 1;
	writeToFile();
	makeGuiText("You lost!", "450 650", "150 30");
	globalFade("myGame:blackBox", 0);
	waitToLoadMenu(3);
}

// Tells the player that the game is ready to start
function clientCmdGameIsReady() {
	$allClientsConnected = true;
}

// Starts the game
function clientCmdStartGame(%map) {
	if(isObject($portText)){
    	$portText.delete();
  	}
    $level = %map;
	globalFade("myGame:blackBox", 1);
}

// Tells the player that their opponent lost connection to the server
function clientCmdOpponentDropped(%playerCount) {
	$playersConnected = 1;
	if(!$IRageQuitted) {
		$clientMessage = "Your opponent was disconnected.";
	}
	$IRageQuitted = false;
	loadMenu(1);
}

// Tells the player that he/she won because your opponent rage quitted
function clientCmdOpponentReturnedToMainMenu() {
	$iHaveWonByOpponentRageQuit += 1;
	writeToFile();
	$playersConnected = 1;
	$clientMessage = "You won! Your opponent returned to the main menu.";
	loadMenu(1);
}

// Tellse the player that he/she lost du to rage quitting
function clientCmdYouReturnedToMainMenu() {
	$iHaveLostByRageQuit += 1;
	writeToFile();
	$IRageQuitted = true;
	$clientMessage = "You lost! You returned to the main menu.";
	
	if($serverLocal) {
		waitToLoadMenu(3);
	} else {
		loadMenu(1);
	}
}

// Slows the player down for a little while
function clientCmdSlowMeDown() {
	$SLOW_DOWN = 10;
	waitForSlowDown(3);
}

// Blocks the players goal for a little while
function clientCmdBlockMyGoal() {
	createBlock();
}