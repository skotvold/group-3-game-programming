// general_functions.cs
// This is necessary because bind() set %val to be 1 or 0 
// 1 = key down, and 0 = key up

function pressPause(%val) {
	 if(%val) {
		togglePauseScene();
		audioManager::pauseToggleAudio();
		
		// print Pause Text
		if($IS_PAUSE) {
			%text = "Game Pause! P to resume, Q to quit";
			$FONTOBJ = imageFont::create(0,10,3,2,%text);
		
		} else {
			imageFont::destroy($FONTOBJ);
		}
	 }
}

//--------------------------------------------------------------------------------

function loadLevel(%var) {

	if(%var) {	
		for(%i = 0; %i < 7; %i++) {
			if(isObject($highscore[%i])){
				$highscore[%i].delete();
			}
		}

		for(%i = 0; %i < 11; %i++) {
			if(isObject($instruction[%i])){
				$instruction[%i].delete();
			}
		}

		menu::destroy();
		menu::startLevel($SCREEN_WIDTH, $SCREEN_HEIGHT);
	}

}
//--------------------------------------------------------------------------------

function loadMenu(%var) {
	if(%var) {
		disconnect();
		$inGame = false;
		level::destroyLevel();
		menu::create($SCREEN_WIDTH, $SCREEN_HEIGHT);
		
		displayMessage();
	}
}

//--------------------------------------------------------------------------------

function IAmAngry(%var) {
	if(%var) {
		commandToServer('RageQuit');
	}
}

//--------------------------------------------------------------------------------

function globalFade(%imageName, %level) {
	// create the fadeMap
	%obj = createFadeMap(%imageName);
	
	// fade in and out
	%obj.fadeInOut(0.05, %level);
	
	// delete object from scene 
	//%obj.safedelete();
}

function globalFadeOut(%imageName, %level) {
	// create the fadeMap
	%obj = createFadeMap(%imageName);
	
	// fade in and out
	%obj.fadeOut(0.95);
	
	// delete object from scene 
	//%obj.safedelete();
}

//--------------------------------------------------------

// Function for splitting string
function explode(%string, %char) {
    if(!isObject(explode)) {
        new ScriptObject(explode);
    }  
  
    %explodeCount = 0;  
    %lastFound = 0;  
  
    %endChar = strLen(%string);   
  
    for(%i=0;%i<%endChar;%i++) {
        %charToCheck = getSubStr(%string, %i, 1);  
        if(%charToCheck $= %char) {  
            explode.contents[%explodeCount] = getSubStr(%string, %lastFound, (%i-%lastFound));   
            %lastFound = %i + 1;  
            %explodeCount++;  
        }
    }  
      
    explode.contents[%explodeCount] = getSubStr(%string, %lastFound, (%i-%lastFound));   
    explode.count = %explodeCount + 1;    
  
    return explode;  
}  



//--------------------------------------------------------------------
//function to use gold

function slowOpponent(%var)
{
	
	if(%var){
		
		if($GOLD_COUNT >= 5){
			$GOLD_COUNT -= 5;
			setGoldText();
			audioManager::playSE("myGame:useGold");
			commandToServer('SlowDown');
		}else{
			// NOT ENOUGH MINERAL/GOLD SOUND! would be awesome^^
		}
	}
	
	
}
function blockOpponent(%var)
{
	if(%var){
		
		if($GOLD_COUNT >= 10){
			$GOLD_COUNT -= 10;
			setGoldText();
			audioManager::playSE("myGame:useGold");
			commandToServer('BlockGoal');
		}else{
			// NOT ENOUGH MINERAL/GOLD SOUND! would be awesome^^
		}
	}
}