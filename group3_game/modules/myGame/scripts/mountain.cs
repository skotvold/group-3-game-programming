//mountain.cs

// Create mountain
function createMountain(%posX, %posY, %sizeX, %sizeY ){   
	// Create the sprite.
	%mountain = new Sprite();

	%mountain.setBodyType( static );

	// Set the position.
	%mountain.setPosition(%posX , %posY);

	// Set the size.        
	%mountain.setSize(%sizeX , %sizeY);

	// Set to the frontmost layer.
	%mountain.SceneLayer = 1;

	//Sets the collision shape to a circle with a radius of 1.5 units
	// this one is wrong atm
    %mountain.Image = "myGame:mountain";

	%mountain.createPolygonBoxCollisionShape();
    
    %mountain.SceneGroup = 2;
 
	// Add the sprite to the scene.
	mySceneWindow.getScene().add( %mountain );
	$Goalobject = %mountain;
}
