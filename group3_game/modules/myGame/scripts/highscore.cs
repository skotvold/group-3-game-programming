//highscore.cs

// create the highscore
function highscore::create_highscore(%width , %height) {
	// create the background [background.cs , first function]
	%obj = background::create(%width,%height,"myGame:menu_bg");
	
	// set new alpha on the player
	%obj.setBlendAlpha(0.7);
	
	// Create a gui text to represent the highscore [guiText.cs , first function]
	$highscore[0] = makeGuiText("HIGHSCORES", "450 60", "150 30");
	$highscore[1] = makeGuiText("I have won a total of " @ $iHaveWon + $iHaveWonByOpponentRageQuit @ " times!", "355 200", "900 30");
	$highscore[2] = makeGuiText("Of those times I have won " @ $iHaveWonByOpponentRageQuit @ " of them by my opponent rage quitting.", "150 250", "900 30");
	$highscore[3] = makeGuiText("I have lost a total of " @ $iHaveLost + $iHaveLostByRageQuit + $iHaveLostByBeingTooSlow @ " times!", "355 350", "900 30");
	$highscore[4] = makeGuiText("Of those times I have lost " @ $iHaveLostByRageQuit @ " of them by rage quitting...", "240 400", "900 30");

	$highscore[5] = makeGuiText("And I have lost " @ $iHaveLostByBeingTooSlow @ " times by just being too slow.", "285 450", "900 30");

	createMenuButton( 0, -25, 45, 7, 11, "myGame:backButton");
}