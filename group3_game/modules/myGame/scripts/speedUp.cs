// Speedup.cs

// create a speed up
function createSpeedUP(%posX, %posY){
	

	// Create the sprite.
    %speedUP = new Sprite(setSpeedUP);

	// set the bodytype of speedUp to be dynamic
    %speedUP.setBodyType( dynamic );

    // Set the position.
    %speedUP.setPosition(%posX , %posY);

    // Set the size.        
    %speedUP.setSize(3, 3);

    // Set to the frontmost layer.
    %speedUP.SceneLayer = 1;
      
	// give the image a sprite
    %speedUP.Image = "myGame:speed";

	// Set the speed up object to the scene group
	%speedUP.SceneGroup = 3;

	// add collision box [polygon] 
    %speedUP.createPolygonBoxCollisionShape();

	
    // Add the sprite to the scene.
	mySceneWindow.getScene().add( %speedUP );
}

// delete a speedup object
function destroySpeedUP(%obj)
{
	%obj.safedelete();
}


