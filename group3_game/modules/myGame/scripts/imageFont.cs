
// imageFont.cs


// Make a simple image font we use in the game : )



function imageFont::create(%posX , %posY , %sizeX, %sizeY, %text)
{
            
    // Create the image font.
    %object = new ImageFont();
    
    // Always try to configure a scene-object prior to adding it to a scene for best performance.
    
    // Set the image font to use the font image asset.
    %object.Image = "ToyAssets:fancyFont";
    
    // We don't really need to do this as the position is set to zero by default.
    %object.setPosition(%posX , %posY);
    
    // We don't need to size this object as it sizes automatically according to the alignment, font-size and text.
   
    // Set the font size in both axis. (world units)
    %object.setFontSize(%sizeX , %sizeY);

    // Set the text alignment.
    %object.TextAlignment = "Center";

    // Set the text to display.
    %object.Text = %text;
    
	// We don't want the font to be touched by physic
    %object.BodyType = "static";
    
    // Add the sprite to the scene.
    mySceneWindow.getScene().add( %object );
	return %object;
}


function imageFont::destroy(%obj)
{
	%obj.delete();
}
