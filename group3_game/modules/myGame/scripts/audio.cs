// audio.cs


// Functions below Handle the background music 
// we can only have one background audio running. 
// ------------------------------------------------------------

// start to play a background music
function audioManager::playBackground(%audioID , %volume)
{
	// set the background audio to global variable
	$musicID = %audioID;
	
	// set the channel volume for background audio
	// the background channel music is always 5 [0,32] channels
	audioManager::setChannelVolume(5, %volume);
	
	// play the background and create the handler
	if(!alxIsPlaying($musicHandle))
	{
		// play music and assign the handler to background music
		$musicHandle = alxPlay($musicID);
	}

}

// stop the background music that is playing
function audioManager::stopBackground()
{
	alxStop($musicHandle);
}

// pause the background music that is playing
function audioManager::pauseToggleAudio()
{

	if(alxIsPlaying($musicHandle))
	{
	
		alxPause($musicHandle);
		
	
	}else{
	
		alxUnpause($musicHandle);
		
	}
	

}
//------------------------------------------------------------





// Sound Effect handling
//--------------------------------------------------------
function audioManager::playSE(%audioID)
{
	if(!$IS_MUTE)
	{
		alxPlay(%audioID);	
	}
}





// Useful utility functions below
//--------------------------------------------------------------------

// set the channel volume, probably not necessary
function audioManager::setChannelVolume(%channelID, %volume)
{
	alxSetChannelVolume(%channelID, %volume);
}


function audioManager::getVolume(%channelID)
{
	return alxGetChannelVolume(%channelID);
}


// stop all handlers to the audio
function audioManager::stopAudio()
{
	alxStopAll();
}

//------------------------------------------------------------------------




//change the user volume
//----------------------------------------------------------------------------------------

$IS_MUTE = false;
$USER_VOLUME_BG = 10;
$USER_VOLUME_SE = 10;


// set the preferred volume down
function volumeDown(%val)
{
	 if(%val && $USER_VOLUME_BG != 0)
	 {
		$USER_VOLUME_BG--;
		$USER_VOLUME_SE --;
		audioManager::setChannelVolume(5, $USER_VOLUME_BG * 0.1);
		audioManager::setChannelVolume(10, $USER_VOLUME_SE * 0.1);
		
	 }
}


// set the preferred volume up
function volumeUp(%val)
{
	if(%val && $USER_VOLUME_BG != 10)
	{
		$USER_VOLUME_BG++;
		$USER_VOLUME_SE += 0.5;
		audioManager::setChannelVolume(5, $USER_VOLUME_BG * 0.1 );
		audioManager::setChannelVolume(10,$USER_VOLUME_SE * 0.1 );
	}
}


// mute the channels
function toggleMute(%val){
	if(%val)
	{

		if(!$IS_MUTE)
		{
			audioManager::setChannelVolume(5,0.0);	
			$IS_MUTE = true;
		
		}
		else
		{
		
			audioManager::setChannelVolume(5, $USER_VOLUME_BG * 0.1 );
			audioManager::setChannelVolume(10,$USER_VOLUME_SE * 0.1 );
			$IS_MUTE = false;
		}
		
	}
}

//----------------------------------------------------------------------------------------



