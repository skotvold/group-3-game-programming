// create a timer
function createTimer (%this){
	$timerOn = true;
}   

// start the timer
function startTimer(%sec, %mes1, %mes2, %text_position , %text_size ,%obj){
	%timer = %mes1 @ %sec;
	
	if(isObject(%obj)){
		%obj.delete();
	}
	
	%obj =  makeGuiText(%timer,%text_position , %text_size);
	%sec -= 1;
	
	if(%sec >= 0 && $timerOn == true)
	{	
		schedule(1000, 0 ,"startTimer", %sec, %mes1, %mes2,%text_position , %text_size, %obj);
	}
	else if($timerOn == false) {
		%obj.delete();
		return;
	}
	else
	{
		makeGuiText(%mes2,%text_position , %text_size);
		%obj.delete();
		commandToServer('TimeOut');
	}
}

// destroy the timer
function destroyTimer() {
	$timerOn = false;
}