
function createScene()
{
    // Destroy the scene if it already exists.
    if ( isObject(mySceneWindow.getScene()) )
        destroyScene();

    // Create the scene.
    new Scene(menuScene);
}


// Not in use
$IS_PAUSE = false;
function togglePauseScene()
{
	// toggle pause
	%scene = mySceneWindow.getScene();
	%scene.setScenePause( !%scene.getScenePause() );
	
	if($IS_PAUSE)
	{
		$IS_PAUSE = false;
	}
	
	else
	{
		$IS_PAUSE = true;
	}
}



