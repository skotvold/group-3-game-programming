// guiText.cs

// create a gui text
function makeGuiText(%text , %position , %size)
{

	// Create a GUI profile, necessary for all GUI-elements
	new GuiControlProfile( textProfile : GuiDefaultProfile ) 
	{  
		fontColor = "255 255 255 255";  
		fontSize = "32";  				
		fontType = "calibri";  			
	};  
	
  // create a new GUI text element
	%obj = new GuiTextCtrl() {  
		Profile = "textProfile";
		Extent=%size;
		Position = %position;		
		Text = %text;
	};  
	
	
	mySceneWindow.addGuiControl( %obj );  
	return %obj;
	
}

// destroy the GUI text
function destroyGuiText(%text)
{
	Canvas.popdialog( %text ); 
}

//-------------------------------------------------------------------------

// Server info
// display server info text
function displayPlayersConnected(%playerCount) {
	if(isObject($con)){
		$con.delete();
	}
	
	if(%playerCount < 2) {
		%text = "Number of connected players: " @ %playerCount;
	} else if (%playerCount == 2 && $serverLocal == true) {
		%text = "Game is ready, press 'PLAY' to start!";
	} else if (%playerCount == 2 && $serverLocal == false) {
		%text = "Game is ready, wait for host to start!";
	}

	$con = makeGuiText(%text, "335 50", "500 30");
}

// Short messages
function displayMessage() {
	if(isObject($DM)){
		$DM.delete();
		$stillActive = false;
	}

	$DM = makeGuiText($clientMessage, "340 30", "500 30");
	$stillActive = true;
	waitForDestroyText(3);
}