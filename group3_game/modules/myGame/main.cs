// Main:
//main.cs



// Create the scenes and games
function myGame::create( %this )
{
	// execute all the scripts
	// scene and window managers
    exec("./gui/guiProfile.cs");
    exec("./scripts/scenewindow.cs");
    exec("./scripts/scene.cs");
	
	// object scripts
	exec("./scripts/background.cs");
	exec("./scripts/dwarf_player.cs");
	exec("./scripts/mountain.cs");
	exec("./scripts/walls.cs");
	exec("./scripts/audio.cs");
	exec("./scripts/main_menu.cs");
	exec("./scripts/fade.cs");
	exec("./scripts/imageFont.cs");
	exec("./scripts/guiText.cs");
	exec("./scripts/speedUP.cs");
	exec("./scripts/push_knight.cs");
	exec("./scripts/gold.cs");
	
    // timer script
    exec("./scripts/timer.cs");

	//control scripts
    exec("./scripts/controls.cs");
	exec("./scripts/level.cs");
	exec("./scripts/menuButton.cs");

	// utility scripts
	exec("./scripts/highscore.cs");
	exec("./scripts/instructions.cs");
	exec("./scripts/wait.cs");
	exec("./scripts/general_functions.cs");

	exec("./scripts/console.cs");
  	TamlRead("./gui/ConsoleDialog.gui.taml"); //Notice we are just reading in the Taml file, not adding it to the scene

	// Network Prefs
  	$pref::Master0 = "2:master.garagegames.com:28002";
  	$pref::Net::LagThreshold = 400;
  	$pref::Net::Port = 28000;
  	$pref::Server::RegionMask = 2;
  	$pref::Net::RegionMask = 2;
  	$pref::Server::Name = "T2D Server";
  	$pref::Player::Name = "T2D Player";

  	// Set up networking
  	setNetPort(0);

  	// Server scripts
	exec("./scripts/Server/server.cs");
	exec("./scripts/Server/serverCommands.cs");
	exec("./scripts/Server/clientConnection.cs");

	// Client scripts
	exec("./scripts/Client/client.cs");
	exec("./scripts/Client/clientCommands.cs");
	exec("./scripts/Client/serverConnection.cs");
	exec("./scripts/Client/fileio.cs");

	// global scene width,height
	$SCREEN_WIDTH  = 200;
	$SCREEN_HEIGHT = 100;

	// Number of connected players are 0 at this point
	$playersConnected = 0;
	
	// Read win/loss stats from local file (TOTALLY UNSAFE!)
	readFromFile();
	
    // Initialize the Scene and Scene window
    createSceneWindow($SCREEN_WIDTH , $SCREEN_HEIGHT); 
    createScene();
    
    // set the scene and create objects
    mySceneWindow.setScene(menuScene);
	audioManager::setChannelVolume( 5 , 1.0 );
	menu::create($SCREEN_WIDTH, $SCREEN_HEIGHT);

	//globalFade("ToyAssets:skyBackground");
	
	new ScriptObject( InputManager );
    mySceneWindow.addInputListener( InputManager );	

}


// Destroy our window and the game

function myGame::destroy( %this )
{
	// destroy the server
	destroyServer();
	
	// destroy the scene
	mySceneWindow.getScene().delete();
	

	// Then quit the game
	quit();
}

