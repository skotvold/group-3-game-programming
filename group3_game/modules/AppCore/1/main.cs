function AppCore::create( %this ){
    // Load system scripts
    exec("./scripts/constants.cs");
    exec("./scripts/defaultPreferences.cs");
    exec("./scripts/canvas.cs");
    exec("./scripts/openal.cs");
    
    //Initialize the canvas
    initializeCanvas("The Lonely Mountain");
    
    // Set the canvas color
    Canvas.BackgroundColor = "InvisibleBlack";
    Canvas.UseBackgroundColor = true;
    
    // Initialize audio
    initializeOpenAL();
    
	
    ModuleDatabase.loadExplicit("myGame");

}


function AppCore::destroy( %this ){
    
}

